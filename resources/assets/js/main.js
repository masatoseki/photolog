$ = window.jQuery = window.$ = require("jquery");

// eliminate 300ms delay in touch UIs
var attachFastClick = require('fastclick');
attachFastClick(document.body);

var waypoints = require("../../../node_modules/waypoints/lib/jquery.waypoints.js");

require('./modules/preloader').init();
require('./modules/responsive-image').init();
require('./modules/hamburger-menu').init();
require('./modules/header-color').init();
require('./modules/no-bottom-space-before-story').init();
require('./modules/pagination').init();