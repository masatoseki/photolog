function init() {
    $('.content-block.story').prev('.content-block').addClass('u-pb0');
    $('.content-block.story').next('.content-block').addClass('u-pt0');
}

exports.init = init;
