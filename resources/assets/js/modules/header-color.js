var init = function() {
    $('.entry-contents, .page-foot').waypoint({
        handler: function(direction) {
            if (direction === 'down') {
                $('.page-head__title').addClass('color-black');
                $('.archive-icon__item').addClass('color-black');
                $('.hamburger-menu span').addClass('color-black');
            } else {
                $('.page-head__title').removeClass('color-black');
                $('.archive-icon__item').removeClass('color-black');
                $('.hamburger-menu span').removeClass('color-black');
            }
        },
        offset: '48px'
    });
    $('.pagination').waypoint({
        handler: function(direction) {
            if (direction === 'down') {
                $('.page-head__title').removeClass('color-black');
                $('.archive-icon__item').removeClass('color-black');
                $('.hamburger-menu span').removeClass('color-black');
            } else {
                $('.page-head__title').addClass('color-black');
                $('.archive-icon__item').addClass('color-black');
                $('.hamburger-menu span').addClass('color-black');
            }
        },
        offset: '48px'
    });
}

exports.init = init;
