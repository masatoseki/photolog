var $hamburgerMenu = $('.hamburger-menu');
var $hamburgerColor = $('.hamburger-menu span');
var $container = $('.container');
var $nav = $('.nav');
var $pageHeadLeft = $('.page-head__left');
var $html = $('html');
var $overlay = $('.overlay');
var $archiveIcon = $('.archive-icon__item');

var toggleContent = function() {
    if ($hamburgerMenu.hasClass('open')) {
        $hamburgerMenu.removeClass('open');
        $hamburgerColor.removeClass('color-white');
        $container.removeClass('pushed');
        $nav.removeClass('pushed');
        $pageHeadLeft.removeClass('pushed');
        $html.removeClass('no-scroll');
        $overlay.removeClass('pushed');
        $archiveIcon.removeClass('open, color-white');
    } else {
        $hamburgerMenu.addClass('open');
        $hamburgerColor.addClass('color-white');
        $container.addClass('pushed');
        $nav.addClass('pushed');
        $pageHeadLeft.addClass('pushed');
        $html.addClass('no-scroll');
        $overlay.addClass('pushed');
        $archiveIcon.addClass('open, color-white');
    }
    return false;
}

var init = function() {
    $hamburgerMenu.on('click', toggleContent);
    $overlay.on('click', toggleContent);
}

exports.init = init;
