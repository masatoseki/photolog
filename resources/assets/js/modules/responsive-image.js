require('imagesloaded');

function init() {
    var min_w = 300; // minimum image width allowed
    var img_w_orig;  // original image dimensions
    var img_h_orig;

    $('.responsive-image').imagesLoaded( function() {
        img_w_orig = parseInt($('.responsive-image__item').attr('width'));
        img_h_orig = parseInt($('.responsive-image__item').attr('height'));

        $(window).resize(function () { resizeToCover(); });
        $(window).trigger('resize');
    });

    function resizeToCover() {
        // set the image viewport to the window size
        $('.responsive-image').width($(window).width());
        $('.responsive-image').height($(window).height());
    
        // use largest scale factor of horizontal/vertical
        var scale_h = $(window).width() / img_w_orig;
        var scale_v = $(window).height() / img_h_orig;
        var scale = scale_h > scale_v ? scale_h : scale_v;
    
        // don't allow scaled width < minimum image width
        if (scale * img_w_orig < min_w) {scale = min_w / img_w_orig;};
    
        // now scale the image
        $('.responsive-image__item').width(scale * img_w_orig);
        $('.responsive-image__item').height(scale * img_h_orig);
        // and center it by scrolling the image viewport
        $('.responsive-image__inner').scrollLeft(($('.responsive-image__item').width() - $(window).width()) / 2);
        $('.responsive-image__inner').scrollTop(($('.responsive-image__item').height() - $(window).height()) / 2);

        $('.responsive-image__item').addClass('show');
    };
}

exports.init = init;
