var Elixir = require('laravel-elixir');

Elixir(function(mix) {
    mix.sass('main.scss')
        .browserify('main.js')
        .browserSync({
            proxy: 'photolog.dev'
        });
});
