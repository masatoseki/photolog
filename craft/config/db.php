<?php

/**
 * Database Configuration
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */


$dev = array(
    'server' => 'localhost',
    'user' => 'root',
    'password' => '',
    'database' => 'photolog_dev');

$staging = array(
    'server' => 'localhost',
    'user' => 'blog',
    'password' => 'KGnDCRxRvvjjJ9rz',
    'database' => 'blog_staging');

$production = array(
    'server' => 'localhost',
    'user' => 'blog',
    'password' => 'KGnDCRxRvvjjJ9rz',
    'database' => 'blog_production');


return array(
    '*' => array(
        'tablePrefix' => 'craft',
    ),
    '.dev' => $dev,
    'localhost' => $dev,
    '192.168.10.10' => $dev,
    'blog-staging.sekkithub.com' => $staging,
    'blog.sekkithub.com' => $production
);
