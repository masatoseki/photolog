<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
    '*' => array(
        'omitScriptNameInUrls' => true,
        'generateTransformsAfterPageLoad' => false,
        'allowAutoUpdates' => false,
        'defaultImageQuality' => 85
    ),

    'localhost' => array(
        'devMode' => true,
        'siteUrl' => 'http://localhost:3000',
        'environment' => 'dev'
    ),

    '.app' => array(
        'devMode' => true,
        'siteUrl' => 'http://photolog.app',
        'environment' => 'dev'
    ),

    'blog-staging.sekkithub.com' => array(
        'cooldownDuration' => 0,
        'siteUrl' => 'http://blog-staging.sekkithub.com',
        'environment' => 'staging'
    ),

    'blog.sekkithub.com' => array(
        'cooldownDuration' => 0,
        'siteUrl' => 'http://blog.sekkithub.com',
        'environment' => 'production'
    ),
);
